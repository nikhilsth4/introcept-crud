import axios from 'axios'

import {
  GET_USER_FAILURE,
  GET_USER_START,
  GET_USER_SUCCESS,
} from './user.types'
import { toast } from 'react-toastify'

// .get(process.env.BASE_URL + 'user')

export const getUsers = async (dispatch) => {
  dispatch(getUserStart())
  await axios
    .get('https://introcept-crud-laravel.herokuapp.com/api/users')
    .then((response) => {
      dispatch({
        type: GET_USER_SUCCESS,
        payload: response.data.data,
      })
    })
    .catch((error) => {
      dispatch(getUserFailure(error.message))
    })
}

//ADD_USER
export const addUser = (user, props, setSubmitting) => async (dispatch) => {
  setSubmitting(true)
  const config = {
    header: {
      'Content-Type': 'application/json',
    },
  }
  await axios
    .post('https://introcept-crud-laravel.herokuapp.com/api/users', user, config)
    .then((response) => {
      props.history.push('/')
      toast.success(response.data.message, {
        position: toast.POSITION.TOP_RIGHT,
      })
      setSubmitting(false)
    })
    .catch((error) => {
      toast.error(error.response.data.message, {
        position: toast.POSITION.TOP_RIGHT,
      })
      setSubmitting(false)
    })
}
export const getUserStart = () => ({
  type: GET_USER_START,
})
export const getUserFailure = (errorMessage) => ({
  type: GET_USER_FAILURE,
  payload: errorMessage,
})
