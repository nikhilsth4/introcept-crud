import {
  GET_USER_START,
  GET_USER_SUCCESS,
  GET_USER_FAILURE,
  ADD_USER,
} from './user.types'

const INITIAL_STATE = {
  users: null,
  isFetching: false,
  errorMessage: undefined,
}

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_USER_START:
      return {
        ...state,
        isFetching: true,
      }
    case GET_USER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        users: action.payload,
        errorMessage: undefined,
      }
    case GET_USER_FAILURE:
      return {
        ...state,
        isFetching: false,
        errorMessage: action.payload,
      }
    case ADD_USER:
      return {
        ...state,
        isFetching: false,
        errorMessage: undefined,
      }
    default:
      return state
  }
}
export default userReducer
