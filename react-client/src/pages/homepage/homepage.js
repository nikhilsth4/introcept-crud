import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { Row } from 'reactstrap'
import Spinner from '../../components/spinner/spinner'

import Users from '../../components/users/Users'
import { getUsers } from '../../redux/user/user.action'
import Pagination from '../../components/pagination/Pagination'

const Homepage = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getUsers)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  const [currentPage, setCurrentPage] = useState(1)
  const [usersPerPage] = useState(3)

  const { users, isFetching } = useSelector((state) => ({
    users: state.user.users,
    isFetching: state.user.isFetching,
  }))

  const indexOfLastUser = currentPage * usersPerPage
  const indexOfFirstUser = indexOfLastUser - usersPerPage
  const currentUsers = users?.slice(indexOfFirstUser, indexOfLastUser)

  const paginate = (pageNumber) => setCurrentPage(pageNumber)

  return (
    <>
      {isFetching ? (
        <Row className={isFetching ? 'justify-content-center' : ''}>
          <Spinner />
        </Row>
      ) : (
        <>
          <Users users={currentUsers} />
          <div className='justify-content'>
            <Pagination
              usersPerPage={usersPerPage}
              totalUsers={users?.length}
              paginate={paginate}
            />
          </div>
        </>
      )}
    </>
  )
}

export default Homepage
