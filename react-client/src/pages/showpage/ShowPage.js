import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Card, CardText, CardTitle, Col, Row } from 'reactstrap'
import { getUsers } from '../../redux/user/user.action'

const ShowPage = (props) => {
  const dispatch = useDispatch()

  const { users } = useSelector((state) => ({
    users: state.user.users,
  }))
  useEffect(() => {
    dispatch(getUsers)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const user = users?.find((user) => user.id === String(props.match.params.id))
  console.log(user)
  return (
    <Card body>
      <Row>
        <Col>
          <CardTitle tag='h5' className='text-capitalize'>
            {user.name}
          </CardTitle>
          <CardText>
            Email : {user.email} <br />
            <div className='text-capitalize'>Gender : {user.gender}</div>
            <div className='text-capitalize'>
              Nationality : {user.nationality}
            </div>
            <div className='text-capitalize'>
              Date Of Birth : {user.dateofbirth}
            </div>
          </CardText>
        </Col>
        <Col>
          <br />
          <CardText>
            <div className='text-capitalize'>
              Education Background : {user.educationbackground}
            </div>
            <div className='text-capitalize'>Phone Number : {user.phone}</div>
            <div className='text-capitalize'>Address : {user.address}</div>
            <div className='text-capitalize'>
              Preffered Contact : {user.prefferedcontact}
            </div>
          </CardText>
        </Col>
      </Row>
    </Card>
  )
}

export default ShowPage
