import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  FormFeedback,
  Row,
  Col,
  Spinner,
} from 'reactstrap'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { Nationality as nationality } from './nationality.data'
import { addUser } from '../../redux/user/user.action'

const Createpage = (props) => {
  const dispatch = useDispatch()
  const [submitting, setSubmitting] = useState(false)
  const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

  const UserSchema = Yup.object().shape({
    name: Yup.string()
      .matches(/^[A-Za-z ]*$/, 'Please enter valid name')
      .max(20, 'Name is too Long!')
      .required('Name is required'),

    email: Yup.string().email('Invalid email').required('Email is Required'),
    phone: Yup.string()
      .matches(phoneRegExp, 'Phone number is not valid')
      .required('Phone Number is Required'),
    address: Yup.string().required('Address is required'),
    nationality: Yup.string().required('Nationality is required'),
    dateOfBirth: Yup.date().required('Date of birth is required'),
    educationBackground: Yup.string().required(
      'Education Background is required'
    ),
    prefferedContact: Yup.string().required('Preffered Contact is required'),
  })
  const formik = useFormik({
    initialValues: {
      name: '',
      gender: 'Male',
      phone: '',
      email: '',
      address: '',
      nationality: '',
      dateOfBirth: '',
      educationBackground: '',
      prefferedContact: '',
    },
    validationSchema: UserSchema,
    onSubmit: (values) => {
      if (!submitting) {
        dispatch(
          addUser(
            {
              name: values.name,
              gender: values.gender,
              phone: values.phone,
              email: values.email,
              address: values.address,
              nationality: values.nationality,
              dateOfBirth: values.dateOfBirth,
              educationBackground: values.educationBackground,
              prefferedContact: values.prefferedContact,
            },
            props,
            setSubmitting
          )
        )
      }
    },
  })
  return (
    <>
      <div className='container'>
        <h1 className='align-items-center'>Add a user</h1>
      </div>
      <Form className='theme-form' onSubmit={formik.handleSubmit}>
        <FormGroup>
          <Label htmlFor='name'>Name</Label>
          <Input
            className={
              formik.touched.name && formik.errors.name
                ? 'form-control is-invalid'
                : 'form-control'
            }
            type='text'
            name='name'
            placeholder='eg Ram Shrestha'
            onChange={formik.handleChange}
            value={formik.values.name}
          />
          {formik.touched.name && formik.errors.name ? (
            <FormFeedback>{formik.errors.name}</FormFeedback>
          ) : null}
        </FormGroup>
        <FormGroup>
          <Row>
            <Col md={3}>
              <Row>
                <Col>
                  <Label htmlFor='Gender'>Gender</Label>
                </Col>
              </Row>
              <Row>
                <Col md={1}></Col>
                <Col>
                  <Input
                    type='radio'
                    name='gender'
                    onChange={formik.handleChange}
                    value='Male'
                    defaultChecked
                  />
                  <Label for='gender'>Male</Label>
                </Col>
                <Col>
                  <Input
                    type='radio'
                    name='gender'
                    onChange={formik.handleChange}
                    value='Female'
                  />
                  <Label for='gender'>Female</Label>
                </Col>
              </Row>
              {formik.touched.gender && formik.errors.gender ? (
                <FormFeedback>{formik.errors.gender}</FormFeedback>
              ) : null}
            </Col>
            <Col md={4}>
              <Label htmlFor='phone'>Phone</Label>
              <Input
                className={
                  formik.touched.phone && formik.errors.phone
                    ? 'form-control is-invalid'
                    : 'form-control'
                }
                type='number'
                name='phone'
                placeholder='eg 9823113123'
                onChange={formik.handleChange}
                value={formik.values.phone}
              />
              {formik.touched.phone && formik.errors.phone ? (
                <FormFeedback>{formik.errors.phone}</FormFeedback>
              ) : null}
            </Col>
            <Col md={5}>
              <Label htmlFor='email'>Email</Label>
              <Input
                className={
                  formik.touched.email && formik.errors.email
                    ? 'form-control is-invalid'
                    : 'form-control'
                }
                type='text'
                name='email'
                placeholder='eg hello@gmail.com'
                onChange={formik.handleChange}
                value={formik.values.email}
              />
              {formik.touched.email && formik.errors.email ? (
                <FormFeedback>{formik.errors.email}</FormFeedback>
              ) : null}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <Row>
            <Col>
              <Label htmlFor='address'>Address</Label>
              <Input
                className={
                  formik.touched.address && formik.errors.address
                    ? 'form-control is-invalid'
                    : 'form-control'
                }
                type='text'
                name='address'
                placeholder='eg Suryabinayak Bhaktapur'
                onChange={formik.handleChange}
                value={formik.values.address}
              />
              {formik.touched.address && formik.errors.address ? (
                <FormFeedback>{formik.errors.address}</FormFeedback>
              ) : null}
            </Col>
            <Col>
              <Label htmlFor='nationality'>Nationality</Label>
              <Input
                className={
                  formik.touched.nationality && formik.errors.nationality
                    ? 'form-control is-invalid'
                    : 'form-control'
                }
                type='select'
                name='nationality'
                placeholder='eg Ram Shrestha'
                onChange={formik.handleChange}
                value={formik.values.nationality}
              >
                <option value='' hidden>
                  Select your nationality
                </option>
                {nationality?.map((nation, index) => {
                  return (
                    <option key={index} value={nation}>
                      {nation}
                    </option>
                  )
                })}
              </Input>
              {formik.touched.nationality && formik.errors.nationality ? (
                <FormFeedback>{formik.errors.nationality}</FormFeedback>
              ) : null}
            </Col>
            <Col>
              <Label htmlFor='dateOfBirth'>Date Of Birth</Label>
              <Input
                className={
                  formik.touched.dateOfBirth && formik.errors.dateOfBirth
                    ? 'form-control is-invalid'
                    : 'form-control'
                }
                type='date'
                name='dateOfBirth'
                onChange={formik.handleChange}
                value={formik.values.dateOfBirth}
              />
              {formik.touched.dateOfBirth && formik.errors.dateOfBirth ? (
                <FormFeedback>{formik.errors.dateOfBirth}</FormFeedback>
              ) : null}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup>
          <Row>
            <Col>
              <Label htmlFor='educationBackground'>Education Background</Label>
              <Input
                className={
                  formik.touched.educationBackground &&
                  formik.errors.educationBackground
                    ? 'form-control is-invalid'
                    : 'form-control'
                }
                type='select'
                name='educationBackground'
                onChange={formik.handleChange}
              >
                <option value='' hidden>
                  Select education background
                </option>
                <option value='elementry'>Elementry</option>
                <option value='high_school'>High School</option>
                <option value='degree'>Degree</option>
              </Input>
              {formik.touched.educationBackground &&
              formik.errors.educationBackground ? (
                <FormFeedback>{formik.errors.educationBackground}</FormFeedback>
              ) : null}
            </Col>
            <Col>
              <Label htmlFor='Preffered'>Preffered Contact</Label>
              <Input
                className={
                  formik.touched.prefferedContact &&
                  formik.errors.prefferedContact
                    ? 'form-control is-invalid'
                    : 'form-control'
                }
                type='select'
                name='prefferedContact'
                onChange={formik.handleChange}
              >
                <option value='' hidden>
                  Select prefferd contact
                </option>
                <option value='email'>Email</option>
                <option value='phone'>Phone</option>
                <option value='none'>None</option>
              </Input>
              {formik.touched.prefferedContact &&
              formik.errors.prefferedContact ? (
                <FormFeedback>{formik.errors.prefferedContact}</FormFeedback>
              ) : null}
            </Col>
          </Row>
        </FormGroup>
        <FormGroup className='form-row mt-3 mb-0 '>
          <Button
            type='submit'
            color='primary'
            className='offset-sm-5'
            disabled={submitting}
          >
            {submitting ? 'Submitting' : 'Submit'}{' '}
            {submitting && <Spinner size='sm' />}
          </Button>
        </FormGroup>
      </Form>
    </>
  )
}

export default Createpage
