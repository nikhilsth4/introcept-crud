import React from 'react'
import { Col, Row } from 'reactstrap'
import UserCard from '../user-card/user-card'

const Users = ({ users }) => {
  return (
    <Row>
      {users?.map((user, key) => {
        return (
          <Col sm='6' md='4' lg='4' className='mb-2'>
            <UserCard user={user} key={key} />
          </Col>
        )
      })}
    </Row>
  )
}

export default Users
