import React from 'react'
import { Spinner } from 'reactstrap'

const spinner = () => {
  return <Spinner style={{ width: '3rem', height: '3rem' }} />
}

export default spinner
