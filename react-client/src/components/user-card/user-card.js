import React from 'react'
import { Card, Button, CardTitle, CardText } from 'reactstrap'
import { withRouter } from 'react-router'

const UserCard = ({ user, history }) => {
  return (
    <Card body>
      <CardTitle tag='h5' className='text-capitalize'>
        {user.name}
      </CardTitle>
      <CardText>
        Email : <span className='text-muted'> {user.email}</span> <br /> Phone
        No : <span className='text-muted'>{user.phone}</span>
      </CardText>
      <Button onClick={() => history.push(`/show/${user.id}`)}>
        View More
      </Button>
    </Card>
  )
}

export default withRouter(UserCard)
