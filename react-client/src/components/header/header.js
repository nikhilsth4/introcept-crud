import React from 'react'
import { Nav, NavItem, NavLink } from 'reactstrap'

const Header = () => {
  return (
    <Nav tabs className='mb-2'>
      <NavItem>
        <NavLink
          href='/'
          active={window.location.pathname === '/' ? true : false}
        >
          Home
        </NavLink>
      </NavItem>
      <NavItem>
        <NavLink
          href='/create'
          active={window.location.pathname === '/create' ? true : false}
        >
          Create
        </NavLink>
      </NavItem>
    </Nav>
  )
}

export default Header
