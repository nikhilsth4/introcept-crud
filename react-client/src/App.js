import { Route, Switch } from 'react-router'
import { Card, CardBody } from 'reactstrap'
import { ToastContainer } from 'react-toastify'

import Header from './components/header/header'
import Createpage from './pages/createpage/CreatePage'
import Homepage from './pages/homepage/HomePage'
import ShowPage from './pages/showpage/ShowPage'

import 'react-toastify/dist/ReactToastify.css'
import './App.css'

const App = () => {
  return (
    <div className='container mt-4'>
      <Card>
        <CardBody>
          <Header />
          <ToastContainer />
          <Switch>
            <Route exact path='/' component={Homepage} />
            <Route exact path='/create' component={Createpage} />
            <Route exact path='/show/:id' component={ShowPage} />
          </Switch>
        </CardBody>
      </Card>
    </div>
  )
}

export default App
