In this application I have used Laravel as Backend and React as a Frontend.

React
*In the begining we land at the home page which consist of various already created users
*We can get the details of the user by clicking on the View More button
*In the Detail Page we view every detail provided by the user
*We can create a user by clicking on the create nav
*Create Page consist form
*For dealing with the form i have used Formik package
*When the user perform unvalidated action i have used formik with Yup package to validate the data and displays the message
*After clicking submit there is also the backend validation like email already exists which is displayed by react-toastify
*If the form donot have any error the data is saved and the user is pushed to the home page with the message user created successfully

Laravel
*I have used laravel 8 as backend
*While saving the backend validation is done and if the data is same as intended then it is saved at csv


Laravel Deployment
*Laravel is deployed using heroku
*At first Procfile is created and the apache server and public directory is initialised
*I created a heroku app introcept-crud-laravel
*I pushed it into heroku

React Deployment
*React is deployed using netlify
*Build version is first created
*It is pushed into netlify
