<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $file = public_path('file/user.csv');
        $csv_data = csvToArray($file);
        if (!empty($csv_data)) {
            return response([
                'success'=> 'true',
                'data'   => $csv_data,
            ], 200);
        } else {
            return response([
                'success'=> 'false',
                'message'=> 'No any users found',
            ], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        function checkEmailExist($email)
        {
            $file = public_path('file/user.csv');

            $csv_data = csvToArray($file);
            if (!($csv_data)) {
                $csv_data = [];
            }
            foreach ($csv_data as $data) {
                if (($data['email'] == $email)) {
                    return true;
                }
            }
        }
        if (!($request->name && $request->gender && $request->phone && $request->email && $request->nationality && $request->dateOfBirth && $request->educationBackground && $request->prefferedContact)) {
            return response([
                'success'=> 'false',
                'message'=> 'Please enter value of every field',
            ], 400);
        }
        if (checkEmailExist($request->email)) {
            return response([
                'success'=> 'false',
                'message'=> 'Email already exists',
            ], 400);
        }
        // return checkEmailExist($request->email);
        try {
            $file = public_path('file/user.csv');

            $csv_data = csvToArray($file);
            if (!($csv_data)) {
                $csv_data = [];
            }

            $file = fopen($file, 'w');

            $data = [
                'id'                  => count($csv_data) + 1,
                'Name'                => $request->name,
                'Gender'              => $request->gender,
                'Phone'               => $request->phone,
                'Email'               => $request->email,
                'Address'             => $request->address,
                'Nationality'         => $request->nationality,
                'DateOfBirth'         => $request->dateOfBirth,
                'EducationBackground' => $request->educationBackground,
                'PrefferedContact'    => $request->prefferedContact,
            ];

            array_push($csv_data, $data);
            $columns = ['id', 'name', 'gender', 'phone', 'email', 'address', 'nationality', 'dateofbirth', 'educationbackground', 'prefferedcontact'];

            fputcsv($file, $columns);
            foreach ($csv_data as $c_data) {
                fputcsv($file, $c_data);
            }
            fclose($file);

            return response(['success'=>'true', 'message'=>'User Added Successfully'], 200);
        } catch (\Exception $ex) {
            return response(['success'=>'false', 'message'=>$ex->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $file = public_path('file/user.csv');
        $csv_data = csvToArray($file);
        foreach ($csv_data as $c_data) {
            if ($c_data['id'] == $id) {
                return $c_data;
            }
        }
    }
}
